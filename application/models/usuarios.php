<?php
class Usuarios extends CI_Model {
	function Usuarios()
	{
		parent::__construct();
	}
	function nombreUsuario($pass,$contra){		
    $this->db->select("nombre('$pass','$contra') nombre",false);
	$datos = $this->db->get();
	$arraydatos = $datos->result_array();
	return $arraydatos[0]['nombre'];	
           }
   function tipoUsuario($pass,$contra){		
    $this->db->select("tipo_usuario('$pass','$contra') tipo_usuario",false);
	$datos = $this->db->get();
	$arraydatos = $datos->result_array();
	return $arraydatos[0]['tipo_usuario'];	
           }   

    function buscarAprendiz($identidad){
$this->db->select('PrimerNombre, PrimerApellido');
	$this->db->from('aprendiz');
	$this->db->where("DocumentoIdentidad =  '".$identidad."'");
	$datos = $this->db->get();
	$arraydatos = $datos->result_array();
	return $arraydatos;	            
}
function listarAprendiz($start,$limit,$like= array()){
$this->db->select('aprendiz.*,nomprograma(IdPrograma) NombrePrograma');
	$this->db->from('aprendiz');
	//$this->db->where("");
	if($like ){
		
		$aux = $like['NombrePrograma'];
		
		unset($like['NombrePrograma']);
	
		$like['nomprograma(IdPrograma)'] = $aux;
		$this->db->like($like);
	}
	$this->db->limit($limit, $start);
	$datos = $this->db->get();
	$arraydatos = $datos->result_array();
	return $arraydatos;	
}
function getTotal($like=array()){
		$this->db->select ("count(*) total",false);
		$this->db->from('aprendiz');
		if($like ){
			
			$aux = $like['NombrePrograma'];
			unset($like['NombrePrograma']);
			$like['nomprograma(IdPrograma)'] = $aux;
			$this->db->like($like);
		}
		$rs = $this->db->get();
		$datos = $rs->result_array();
		return $datos[0]['total'];
	}
	function getDatosReporteAprendiz($like){
$this->db->select('aprendiz.*,nomprograma(IdPrograma) NombrePrograma');
	$this->db->from('aprendiz');
	if($like){
		$aux = $like['NombrePrograma'];
		unset($like['NombrePrograma']);
		$like['nomprograma(IdPrograma)'] = $aux;
		$this->db->like($like);
	}
	$datos = $this->db->get();
	$arraydatos = $datos->result_array();
	return $arraydatos;	
}

function insertarAprendiz($nomprograma,$estado,$pnombre,$snombre,$papellido,$sapellido,$identificacion,$fnacimeineto,$fingreso,$direccion,$telefono,$email){
	$this->db->select("insertarAprendiz('$nomprograma','$estado','$pnombre','$snombre','$papellido','$sapellido','$identificacion','$fnacimeineto','$fingreso','$direccion','$telefono','$email') insertaAprendiz",false);
	$datos = $this->db->get();
	$arraydatos = $datos->result_array();
	return $arraydatos[0]['insertaAprendiz'];	
}
}