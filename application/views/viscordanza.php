<html>
<head>
		<title>Bienestar Sena Colombo Alem&aacute;n</title>
        <meta charset="utf-8"/>
        <meta name="description" content="Bienvenido al Bienestar del Aprendiz Sena Colombo Alemán" />
        <meta name="viewport" content="width=device-width,initial-scale=1"/> 
        <script type="text/javascript">
            bienestar = {};
            bienestar.base_url = "<?php echo base_url(); ?>";
            bienestar.urlcerrarSesion = bienestar.base_url + "index.php/conprincipal/cerrarSesion";           
    </script>
        <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/ext4/ext-all-dev.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/jquery-1.10.2.js"></script>      
       <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/cerrarsesion.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/jquery.min.js"></script>
         <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/jquery.flexslider.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/menu_jquery.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>recursos/css/estilo.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>recursos/css/Estilo_acordeon.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>recursos/css/flexslider.css" />
        <script>
		$(window).load(function(){
			$('.flexslider').flexslider();
		});
		</script>
</head>
<body>
<header>
            <?php include("recursos/includes/cabecera.php");?>
        <nav>
            
        </nav>
            <div id="usuario">
               
                    <?php                     
                    echo "Bienvenido:"." ".$nombreUsuario."  ";
                     echo "  ". '<a href="#" onclick="bienestar.cerrarSesion();">Cerrar Sesion</a>';
                     ?> 
            </div>
    </header>
        <section id="cuerpo">
                <section id="bloqueiz">
                    <article>
                        <?php include("recursos/includes/menuinsdanza.php");?>
                    </article>
                    
                </section>
                <section id="bloquecentral">
                    <article>
                               ADMINISTRACION DANZA
                    </article>
                   
                </section>
                <section id="bloquederecha">
                    
                </section>
                <section id="bloquenoticias">
                    
                </section>
                    
        </section>
        <footer>
            Pie de Pagina
        </footer>
</body>
</html>