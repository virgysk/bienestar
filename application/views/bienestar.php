<!DOCTYPE html>
<html>
<head>
		<title>Bienestar Sena Colombo Alem&aacute;n</title>
        <meta charset="utf-8"/>
        <meta name="description" content="Bienvenido al Bienestar del Aprendiz Sena Colombo Alemán" />
        <meta name="viewport" content="width=device-width,initial-scale=1"/>
        <script type="text/javascript">
            bienestar = {};
            bienestar.base_url = "<?php echo base_url(); ?>";
           bienestar.urlLogin = bienestar.base_url + "index.php/conprincipal/iniciarSesion";
            bienestar.urlRegistro = bienestar.base_url + "index.php/conusuario/registrar"
        </script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>recursos/js/ext4/resources/css/ext-all.css"/>
        <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/ext4/ext-all-dev.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/login.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/jquery.min.js"></script>
         <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/jquery.flexslider.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>recursos/js/menu_jquery.js"></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>recursos/css/estilos.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>recursos/css/Estilo_acordeon.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>recursos/css/flexslider.css" />
        <script>
		$(window).load(function(){
			$('.flexslider').flexslider();
		});
		</script>
</head>
<body>
<header>
  <h1><?php include("recursos/includes/cabecera.php"); ?></h1>
    <nav>
    	<ul>
        	<li><a href="views/bienestar.php">Inicio</a>
            <li><a href="<?php echo base_url(); ?>index.php/conprincipal/listarAprendiz">Deporte</a>
           	<li><a href="#">Arte y Cultura</a>
            <li><a href="#">Desarrollo Intelectual</a>
            <li><a href="#">Promoci&oacute;n Socioecon&oacute;mica</a>
            <li><a href="#">Salud</a>
            <li><a href="#">Integridad de la Formaci&oacute;n</a>
            <li><a  href="#" name="btnLogin" id="btnLogin">Login</a>
    </nav>
</header>
		<section id="contenido">
         			<section id="menu_izquierda">
                    	<article id="menu">
                             <?php include("recursos/includes/menuvertical.php"); ?>                
                        </article>
                    </section>
                    <section id="menu_principal">
                        <article id="menu_slider">
                        	 <?php include("recursos/includes/menu_slider.php"); ?>
                        </article>
                    </section>
</body>
</html>