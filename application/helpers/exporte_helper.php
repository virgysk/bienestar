<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('exportarUsuarios')) {

  function exportarUsuarios($datosReporte){
      $CI = &get_instance();
      $CI->pdf->SetSubject('Reporte Usuarios');
      $CI->pdf->SetHeaderMargin(0);
      $CI->pdf->SetFont('times', '', 10);
      $CI->pdf->AddPage();

      $contenido = '<style type="text/css">
      .tabla {
        padding: 5px;
        text-align: justify;
        border-bottom: black solid thin;
        border-left: black solid thin;
        border-right: black solid thin;
        border-top: black solid thin;
      }

    .tabla td {
      padding: 5px;
      border-bottom: black solid thin;
      border-left: black solid thin;
      border-right: black solid thin;
      border-top: black solid thin;
      }
    </style>';
     
    $contenido = $contenido."<h4>Listado de Usuarios:</h4><BR><BR>";

    $contenido = $contenido.'<table class="tabla">';
    $contenido = $contenido.'<tr>';
    $contenido = $contenido. "<td><strong>Programa</strong></td>";
    $contenido = $contenido. "<td><strong>Primer Nombre</strong></td>";
    $contenido = $contenido. "<td><strong>Segundo Nombre </strong></td>";
    $contenido = $contenido. "<td><strong>Primer Apellido</strong></td>";
    $contenido = $contenido. "<td><strong>Segundo Apellido</strong></td>";
    $contenido = $contenido. "<td><strong>Documento</strong></td>";
    $contenido = $contenido. "<td><strong>Fecha Nacimiento</strong></td>";
    $contenido = $contenido. "<td><strong>Fecha Ingreso</strong></td>";
    $contenido = $contenido. "<td><strong>Direccion</strong></td>";
    $contenido = $contenido. "<td><strong>Telefono</strong></td>";
    $contenido = $contenido. "<td><strong>Email</strong></td>";
    $contenido=$contenido."</tr>";

    $total = count($datosReporte);
    for ($i=0; $i < $total; $i++) { 
          $contenido = $contenido.'<tr>';
          $contenido = $contenido. "<td>".$datosReporte[$i]["NombrePrograma"]."</td>";
          $contenido = $contenido. "<td>".$datosReporte[$i]["PrimerNombre"]."</td>";
          $contenido = $contenido. "<td>".$datosReporte[$i]["SegundoNombre"]."</td>";
          $contenido = $contenido. "<td>".$datosReporte[$i]["PrimerApellido"]."</td>";
          $contenido = $contenido. "<td>".$datosReporte[$i]["SegundoApellido"]."</td>";
           $contenido = $contenido. "<td>".$datosReporte[$i]["SegundoApellido"]."</td>";
          $contenido = $contenido. "<td>".$datosReporte[$i]["FechaNacimiento"]."</td>";
          $contenido = $contenido. "<td>".$datosReporte[$i]["FechaIngreso"]."</td>";
          $contenido = $contenido. "<td>".$datosReporte[$i]["Direccion"]."</td>";
          $contenido = $contenido. "<td>".$datosReporte[$i]["Telefono"]."</td>";
          $contenido = $contenido. "<td>".$datosReporte[$i]["Email"]."</td>";
          $contenido=$contenido."</tr>";
    }

    
    $contenido = $contenido."</table>";

    $CI->pdf->writeHTML($contenido, true, false, false, false, '');
    $CI->pdf->Output('reporte_usuarios.pdf', 'I');
    }
}