<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conprincipal extends CI_Controller {

	
	public function index()
	{
		$usuario = $this->session->userdata('usuario');
		$datotipousuario = $this->session->userdata('tipousuario');
		$nombre = $this->session->userdata('nombreusuario');
		$variables = array('nombreUsuario' => $nombre);
		if($usuario){
			if($datotipousuario == "administrador"){
				$this->load->view('visadministrador',$variables);
			       }
			else if($datotipousuario == "aprendiz"){
				$this->load->view('visaprendiz',$variables);
			        }
			  else if($datotipousuario == "instructor"){
				$this->load->view('visinstructor',$variables);
			        } 
			   else if($datotipousuario == "coordinadordeporte"){
				$this->load->view('viscordeporte',$variables);
			        }              
			   else if($datotipousuario == "orientacionconcejeria"){
				$this->load->view('visconceorientacion',$variables);
			        }  
			    else if($datotipousuario == "coordinadordanza"){
				$this->load->view('viscordanza',$variables);
			        } 
			     else if($datotipousuario == "desarrollointelectual"){
				$this->load->view('visdesaintelectual',$variables);
			        }
			      else if($datotipousuario == "relacionescorporativas"){
				$this->load->view('visrelacorporativas',$variables);
			        }                          
		    }
			else {	   
		        $this->load->view('bienestar');
		          }
		
	}
	
	public function iniciarSesion(){
		$pass = $this->input->post('usuario');
		$contra = $this->input->post('contrasena');

		$this->load->model("usuarios");
		$datotipoUsuario = $this->usuarios->tipoUsuario($pass,$contra);
		$nombreUsuario = $this->usuarios->nombreUsuario($pass,$contra);
		if($datotipoUsuario == 'administrador'){
			$datosSesion = array('usuario' => $pass,				
				'tipousuario'=>$datotipoUsuario,
				'logged_in' => TRUE,
				'nombreusuario' => $nombreUsuario );
			$this->session->set_userdata($datosSesion);
			echo json_encode(array('success' => true,'mensaje'=>"logueado exitoso",
				'TipoUsuario'=> $datotipoUsuario));
		}
		else if($datotipoUsuario == 'aprendiz'){
			$datosSesion = array('usuario' => $pass,				
				'tipousuario'=>$datotipoUsuario,
				'logged_in' => TRUE ,'nombreusuario' => $nombreUsuario );
			$this->session->set_userdata($datosSesion);
			echo json_encode(array('success' => true,'mensaje'=>"logueado exitoso",
				'TipoUsuario'=> $datotipoUsuario));
		}
		else if($datotipoUsuario == 'instructor'){
			$datosSesion = array('usuario' => $pass,				
				'tipousuario'=>$datotipoUsuario,
				'logged_in' => TRUE ,'nombreusuario' => $nombreUsuario );
			$this->session->set_userdata($datosSesion);
			echo json_encode(array('success' => true,'mensaje'=>"logueado exitoso",
				'TipoUsuario'=> $datotipoUsuario));
		}
		else if($datotipoUsuario == 'coordinadordeporte'){
			$datosSesion = array('usuario' => $pass,				
				'tipousuario'=>$datotipoUsuario,
				'logged_in' => TRUE ,'nombreusuario' => $nombreUsuario );
			$this->session->set_userdata($datosSesion);
			echo json_encode(array('success' => true,'mensaje'=>"logueado exitoso",
				'TipoUsuario'=> $datotipoUsuario));
		}    
		else if($datotipoUsuario == 'orientacionconcejeria'){
			$datosSesion = array('usuario' => $pass,				
				'tipousuario'=>$datotipoUsuario,
				'logged_in' => TRUE ,'nombreusuario' => $nombreUsuario );
			$this->session->set_userdata($datosSesion);
			echo json_encode(array('success' => true,'mensaje'=>"logueado exitoso",
				'TipoUsuario'=> $datotipoUsuario));
		}    
		else if($datotipoUsuario == 'coordinadordanza'){
			$datosSesion = array('usuario' => $pass,				
				'tipousuario'=>$datotipoUsuario,
				'logged_in' => TRUE ,'nombreusuario' => $nombreUsuario );
			$this->session->set_userdata($datosSesion);
			echo json_encode(array('success' => true,'mensaje'=>"logueado exitoso",
				'TipoUsuario'=> $datotipoUsuario));
		} 
		else if($datotipoUsuario == 'desarrollointelectual'){
			$datosSesion = array('usuario' => $pass,				
				'tipousuario'=>$datotipoUsuario,
				'logged_in' => TRUE ,'nombreusuario' => $nombreUsuario );
			$this->session->set_userdata($datosSesion);
			echo json_encode(array('success' => true,'mensaje'=>"logueado exitoso",
				'TipoUsuario'=> $datotipoUsuario));
		} 
		else if($datotipoUsuario == 'relacionescorporativas'){
			$datosSesion = array('usuario' => $pass,				
				'tipousuario'=>$datotipoUsuario,
				'logged_in' => TRUE ,'nombreusuario' => $nombreUsuario );
			$this->session->set_userdata($datosSesion);
			echo json_encode(array('success' => true,'mensaje'=>"logueado exitoso",
				'TipoUsuario'=> $datotipoUsuario));
		}             
		else{
			echo json_encode(array('success' => false,'mensaje'=>"datos incorrectos" ));
		}
	}
	public function cerrarSesion(){
		$this->session->sess_destroy();
		echo json_encode(array('success' => true));
		//$this->index();

	}
	public function buscarUsuario(){
		$identidad = $this->input->post('identidad');

		$this->load->model("usuarios");
		$datosUsuario = $this->usuarios->buscarAprendiz($identidad);
		if(count($datosUsuario) > 0){			
			echo json_encode(array('success' => true,'mensaje'=>"logueado exitoso",
				'PrimerNombre'=> $datosUsuario[0]['PrimerNombre'], 'PrimerApellido'=> $datosUsuario[0]['PrimerApellido']));
		}else{
			echo json_encode(array('success' => false,'mensaje'=>"datos incorrectos" ));
		}

	}
    public function listarAprendiz(){
		$this->load->model("usuarios");
        $start =$_POST['start'];
	    $limit = $_POST['limit'];
        

		$search = isset($_POST['search']) ? json_decode($_POST['search'],true) : array();
		$datos = $this->usuarios->listarAprendiz($start,$limit,$search);
		$total = $this->usuarios->getTotal($search);
		//$this->load->view("listar",$datos);
		echo json_encode(array('success' => true, 'datos'=>$datos,'total'=>$total));
	}
	public function administrador(){
		$nombre = $this->session->userdata('nombreusuario');
		$variables = array('nombreUsuario' => $nombre);
		$this->load->view("visadministrador",$variables);
	}
	public function instructor(){
		$nombre = $this->session->userdata('nombreusuario');
		$variables = array('nombreUsuario' => $nombre);
		$this->load->view("visinstructor",$variables);
	}
	public function coordinadordeporte(){
		$nombre = $this->session->userdata('nombreusuario');
		$variables = array('nombreUsuario' => $nombre);
		$this->load->view("viscordeporte",$variables);
	}
	public function orientacionconcejeria(){
		$nombre = $this->session->userdata('nombreusuario');
		$variables = array('nombreUsuario' => $nombre);
		$this->load->view("visconceorientacion",$variables);
	}
	public function coordinadordanza(){
		$nombre = $this->session->userdata('nombreusuario');
		$variables = array('nombreUsuario' => $nombre);
		$this->load->view("viscordanza",$variables);
	}
	public function desarrollointelectual(){
		$nombre = $this->session->userdata('nombreusuario');
		$variables = array('nombreUsuario' => $nombre);
		$this->load->view("visdesaintelectual",$variables);
	}
	public function relacionescorporativas(){
		$nombre = $this->session->userdata('nombreusuario');
		$variables = array('nombreUsuario' => $nombre);
		$this->load->view("visrelacorporativas",$variables);
	}
	public function aprendiz(){
		$nombre = $this->session->userdata('nombreusuario');
		$variables = array('nombreUsuario' => $nombre);
		$this->load->view("visaprendiz",$variables);
	}
	public function exportarAprendiz(){
		$this->load->model("usuarios");
		$search = isset($_GET['search']) ? json_decode($_GET['search'],true) : array();
		$datosReporte = $this->usuarios->getDatosReporteAprendiz($search);

		exportarUsuarios($datosReporte);
	}
	public function importarAprendiz(){
		$config['file_name'] = "datos";
		$config['upload_path'] = './upload';
		$config['allowed_types'] = 'xls|xlsx';
		$config['max_size'] = '5120';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
        $errores = "parte1";
		$this -> load -> library('upload', $config);
		if (!$this -> upload -> do_upload('file')) {
			//$error = array('error' => $this -> upload -> display_errors());
			$mjs = $this -> upload -> display_errors();
			$mjs = str_replace("<p>", "", $mjs);
			$mjs = str_replace("</p>", "", $mjs);			
			echo json_encode(array('success' => false, 'mensaje'=>$mjs));
			$errores = "parte2";
		} else {
			$data = $this -> upload -> data();
			$inputFileName = $data['full_path'];
			$this->load->library('excel');
			$inputFileType = 'Excel5';
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($inputFileName);
			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

			$i = 1;
			$this->load->model('usuarios');
			$this->load->helper('email');

			$errores = "";
			foreach ($sheetData as $key => $value) {
				if($i>1){
					if(valid_email($value['L'])==TRUE){
						$resp = $this->usuarios->insertarAprendiz($value['A'],$value['B'],$value['C'],$value['D'],$value['E'],$value['F'],$value['G'],$value['H'],$value['I'],$value['J'],$value['K'],$value['L']);
						if($resp==0){
							$fecha=$value['H'];
							$errores =$errores."<p>El usuario en la fila ".$i." ya existe</p>";
						}
					}else{
						$errores =$errores."<p>El email de la fila ".$i." no es valido</p>";
					}
				}
				$i=$i+1;
			}
			unlink($inputFileName);
			
			echo json_encode(array('success' => true, 'errores'=>$errores));
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */