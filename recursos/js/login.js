Ext.onReady(function(){
				Ext.get("btnLogin").on('click',function(){
					var ventanaLogin = Ext.create("Ext.window.Window",{
						title: "Login",
						modal: true,
						width: 300,
        				height: 200,
        				layout:'fit',
        				items:[
        				{
                            xtype: 'form',
                            frame:true,
                            name: "frmLogin",
                            items:[
                            	{
                                       xtype: 'textfield',
                                       flex:1,
                                       padding: 10,
                                       labelWidth: 70,
                                       name: 'txtUsuario',
                                       emptyText: 'Usuario',
                                       fieldLabel: 'Usuario',
                                       allowBlank: false
                                   },
                                    {
                                        xtype: 'textfield',
                                        inputType: 'password',
                                        padding: 10,
                                        flex:1,
                                        name: 'txtContrasena',
                                        emptyText: 'Clave',
                                        fieldLabel: 'Password',
                                        labelWidth: 70,
                                        allowBlank: false
                                    },{
                                    	xtype: 'panel',
                                    	html: "Datos incorrectos",
                                    	name: 'pnError',
                                    	hidden: true,
                                    	bodyStyle: 'background:#DFE9F6; color:red'
                                    }
                                    ,{
                                        xtype: 'panel',
                                        html: "Datos correctos",
                                        name: 'pnBien',
                                        hidden: true,
                                        bodyStyle: 'background:#DFE9F6; color:red'
                                    }
                            ],
                           buttons: [
                                {
                                    xtype: 'button',
                                    text: 'Login',
                                    tooltip: 'Iniciar Sesion',
                                    name: 'btnIniciarSesion',
                                    formBind: true,
                                    handler: function(boton){
                                    	var formulario = boton.up("form[name=frmLogin]");
                                    	console.log(formulario);
                                    	var usuario = formulario.down("textfield[name=txtUsuario]").getValue();
										var contrasena = formulario.down("textfield[name=txtContrasena]").getValue();
                                    	var datos = {'usuario':usuario, 'contrasena':contrasena};
                                    	Ext.Ajax.request({
                                    		type: 'post',
											url: bienestar.urlLogin,
											params: datos,
											success:function(respuesta, peticion){
												var obj = Ext.decode(respuesta.responseText);
												if(obj.success){
													location.href= bienestar.base_url + "index.php/conprincipal/"+obj.TipoUsuario;
												}else{
													formulario.down("panel[name=pnError]").show();
												}
											},
											error: function(respuesta, peticion){
												console.log(respuesta);
											}
										});
                                    }
                                }
                            ]
                        }
        				]
					}).show();
				});
			});