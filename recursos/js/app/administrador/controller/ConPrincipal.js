Ext.define('administrador.controller.ConPrincipal', {
    extend: 'Ext.app.Controller',
    views: ['VisPrincipal','FormCargas'],
    stores:['StoAprendiz'],
    models: [],

    init: function () {
        this.control({
        	'visprincipal button[name=btnCerrarSesion]': {
                click:this.cerrarSesion
            },
            'visprincipal button[name=btnGestionFuncionario]': {
                click:this.mostrarVistaParametros
            },
            'visprincipal button[name=btnGestionAprendiz]': {
                click:this.mostrarVistaAprendiz
            }
    })
    },

    cerrarSesion: function(boton){
        Ext.Ajax.request({
            type: 'post',
            url: bienestar.Utilidades.urlCerrarSesion,
            success:function(respuesta, peticion){
                var obj = Ext.decode(respuesta.responseText);
                if(obj.success){
                    location.href= bienestar.base_url;
                }
            },
                error: function(respuesta, peticion){
                    console.log(respuesta);
                }
            });
    },

    mostrarVistaAprendiz: function(boton){
        var visPrincipal = boton.up('visprincipal');
        var tabPanel = visPrincipal.down('tabpanel[name=tbPrincipal]');
        var tabUsuario = tabPanel.down('panel[name=tabUsuarios]');
        if(!tabUsuario){
            var tabUsuario = Ext.create('administrador.view.VisUsuario',{
            closable: true,
            titulo: 'Aprendiz',
            name: 'tabUsuarios'
            });
            tabPanel.add(tabUsuario);
        }
        tabPanel.setActiveTab(tabUsuario);
    },
    mostrarVistaParametros: function(boton){
        Ext.Msg.alert("Parametros","Mostrar vista parametros");
    }

});