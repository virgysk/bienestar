Ext.define('administrador.controller.ConUsuario', {
    extend: 'Ext.app.Controller',
    views: ['VisUsuario','FormUsuario'],
    stores:['StoAprendiz'],
    models: [],

     refs: [{
         ref: 'usuario',
         selector: 'visusuario'
     }],

    init: function () {
        this.control({
            'visusuario actioncolumn[name=btnEditarUsuario]': {
                click:this.editarUsuario
            },

            'visusuario button[name=btnImportarUsuario]': {
                click:this.importarAprendiz
            },

            'visusuario button[name=btnNuevoUsuario]': {
                click:this.insertarUsuario
            },
            'visusuario button[name=btnExportarUsuario]': {
                click:this.exportarUsuarios
            },

            'formcargas button[name=btnCargarAprendiz]': {
                click:this.subirArchivoAprendiz
            }

    })
    },

 exportarUsuarios: function(boton){
    console.log('llegaste');
        var grid = this.getUsuario().down("gridpanel[name=grdUsuarios]");
        var storeUsuarios = grid.getStore();

        if( storeUsuarios.count() == 0){
            Ext.Msg.alert("Información","No hay registros para exportar");
        }else{
            var search = storeUsuarios.proxy.extraParams.search==null?"":"?search="+storeUsuarios.proxy.extraParams.search;
            window.open(bienestar.Utilidades.urlExportarAprendiz+search);
        }
    },
   insertarUsuario: function(boton){
    var vista = Ext.widget('formusuario',{
            opcion: 'insercion'
        });
    },

    editarUsuario: function(grid, view, recordIndex, cellIndex, item, record){
        var vista = Ext.widget('formusuario',{
            opcion: 'edicion'
        });
        vista.down('form').loadRecord(record);
    },

    importarAprendiz:function(boton){
-       console.log('llegaste'); 
         Ext.widget('formcargas',{        
             modulo: 'Aprendiz'
         }).show();
    },
    subirArchivoAprendiz: function(boton){

       
        var grid = this.getUsuario().down("gridpanel[name=grdUsuarios]");
        var form = boton.up('form').getForm();
        var ventana = boton.up("formcargas");
       
            if(form.isValid()){
                form.submit({
                method : 'POST',
                url : bienestar.Utilidades.urlImportarAprendiz,
                waitMsg : 'Cargando virgy...',
                success : function(form, action) {
                    var obj = Ext.decode(action.response.responseText);
                    ventana.close();
                    grid.getStore().load();
                    if(obj.errores!=""){
                        Ext.Msg.alert('Errores', obj.errores);
                    }
                },
                failure : function(form, action) {
                   
                    if (action.failureType === 'server') {

                        Ext.Msg.alert('Error', 'Archivo invalido verifique tamaño o extension');
                    }

                }

            });
            }
    }
});