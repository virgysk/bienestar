Ext.define('administrador.view.FormCargas', {
    extend: 'Ext.window.Window',
    alias: 'widget.formcargas',
    height: 100,
    width: 366,
    modal: true,
    resizable: false,
    constrainHeader: true,
    layout: {
        type: 'auto'
    },
    title: 'Carga Masiva',
    modulo: '',
    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    title: '',
                    buttonAlign: 'right',
                    items: [

                        {
                            xtype: 'filefield',
                            anchor: '100%',
                            name: 'file',
                            allowBlank: false,
                            blankText: 'Este campo es requerido',
                            fieldLabel: '',
                            emptyText: 'Seleccione su Archivo',
                            buttonText: 'Examinar',
                            regex : /^(.+\.xls|.+\.xlsx)$/,
                            regexText : 'Solo se aceptan archivos con extension .xls y .xlsx',
                        },
                        {
                            xtype: 'button',
                            formBind: true,
                            text: 'Cargar',
                            name: 'btnCargar'+me.modulo
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});