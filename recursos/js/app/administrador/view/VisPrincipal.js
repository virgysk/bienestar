Ext.define('administrador.view.VisPrincipal', {
    extend: 'Ext.form.Panel',
    alias: 'widget.visprincipal',
    autoShow: true,
    layout: {
        type: 'border'
    },
    bodyPadding: '',
    title: 'Administración',
    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
      
                {
                    xtype: 'tabpanel',
                    activeTab: 0,
                    name: 'tbPrincipal',
                    region: 'center',
                    items: [
                        {
                            xtype: 'panel',
                            title: 'Inicio',
                            html: 'bienestar'
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    width: 80,
                    dock: 'top',
                    collapsible: true,
                    items: [
                        {
                            xtype: 'button',
                            height: 60,
                            width: 80,
                            text: 'Funcionarios',
                            tooltip: 'Gestión de Funcionario',
                            name: 'btnGestionFuncionarios',
                            icon: bienestar.Utilidades.iconoGestionParametros,
                            iconAlign: 'top',
                            scale: 'large'
                        },
                        {
                            xtype: 'button',
                            height: 60,
                            width: 80,
                            text: 'Aprendiz',
                            tooltip: 'Gestión de Aprendiz',
                            name: 'btnGestionAprendiz',
                            icon: bienestar.Utilidades.iconoGestionUsuarios,
                            iconAlign: 'top',
                            scale: 'large'
                        },
                        {
                            xtype: 'button',
                            height: 60,
                            width: 80,
                            text: 'Cerrar Sesión',
                            tooltip: 'Cerrar Sesión',
                            name: 'btnCerrarSesion',
                            icon: bienestar.Utilidades.iconoCerrarSesion,
                            iconAlign: 'top',
                            scale: 'large'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});