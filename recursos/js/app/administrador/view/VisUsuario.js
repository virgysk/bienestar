Ext.define('administrador.view.VisUsuario', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.visusuario',
    titulo: '',
    closable: false,
    layout: {
        align: 'stretch',
        type: 'hbox'
    },
    flex: 1,
    name: '',
    initComponent: function() {
        var me = this;
        var selModel = Ext.create('Ext.selection.CheckboxModel');
        Ext.applyIf(me, {
            title: me.titulo,
            closable: me.closable,
            items: [
                {
                    xtype: 'gridpanel',
                    name: 'grdUsuarios',
                    title: 'Lista Usuarios',
                    store: 'StoAprendiz',
                    flex: 1,
                    multiSelect: true,
                    selModel: selModel,
                    plugins: [Ext.create('bienestar.utilidades.FilterRow',{remoteFilter:true})],
                    columns: [
                        Ext.create('Ext.grid.RowNumberer',{
                            xfilter: {
                                disabled: true
                            }
                        }),
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'NombrePrograma',
                            text: 'Programa',
                            flex: 2
                        },
                       
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'PrimerNombre',
                            text: 'PrimerNombre',
                            flex: 1,
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'SegundoNombre',
                            text: 'SegundoNombre',
                            flex: 1,
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'PrimerApellido',
                            text: 'PrimerApellido',
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'SegundoApellido',
                            text: 'SegundoApellido',
                            flex: 1,
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'DocumentoIdentidad',
                            text: 'Identificacion',
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'FechaNacimiento',
                            text: 'FechaNacimiento',
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'FechaIngreso',
                            text: 'FechaIngreso',
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'Direccion',
                            text: 'Direccion',
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'Telefono',
                            text: 'Telefono',
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'Email',
                            text: 'Email',
                            flex: 2
                        },{
                            xtype: 'actioncolumn',
                            name: 'btnEditarUsuario',
                            text: 'Editar',
                            align: 'center',
                            icon: bienestar.Utilidades.iconoEdicionGrilla,
                            xfilter: {
                                disabled: true
                            }
                        },{
                            xtype: 'actioncolumn',
                            name: 'btnEliminarUsuario',
                            text: 'Eliminar',
                            align: 'center',
                            icon: bienestar.Utilidades.iconoEliminacionGrilla,
                            xfilter: {
                                disabled: true
                            }
                        }
                    ],
                    dockedItems: [
                                    {
                                        xtype: 'toolbar',
                                        dock: 'bottom',
                                        items: [
                                        {
                                            xtype: 'pagingtoolbar',
                                            flex: 1,
                                            displayInfo: true,
                                            store: 'StoAprendiz',
                                            displayMsg: 'Usuarios' +' {0} - {1} de {2}',
                                            emptyMsg: "No hay usuarios disponibles"
                                        }
                                        ]
                                    },
                                    {
                                            xtype: 'toolbar',
                                            dock: 'top',
                                            items:[
                                                {
                                                    xtype: 'button',
                                                    text: 'Nuevo',
                                                    name: 'btnNuevoUsuario',
                                                    icon: bienestar.Utilidades.iconoInsercionGrilla
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Importar',
                                                    name: 'btnImportarUsuario',
                                                    icon: bienestar.Utilidades.iconoImportacionGrilla
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Exportar',
                                                    name: 'btnExportarUsuario',
                                                    icon: bienestar.Utilidades.iconoPdf
                                                }
                                            ]
                                        }
                                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});