Ext.define('administrador.view.FormUsuario', {
    extend: 'Ext.window.Window',
    alias: 'widget.formusuario',
    height: 230,
    width: 400,
    autoShow: true,
    layout: {
       // align: 'stretch',
        //pack: 'center',
        type: 'fit'
    },
    title: 'Editar Usuario',
    modal: true,
    opcion: 'insercion',
    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    height: 100,
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldLabel: 'IdPrograma',
                            name: 'IdPrograma',
                            readOnly: true,
                            hidden: (me.opcion=='insercion'?true:false),
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldLabel: 'PrimerNombre',
                            name: 'PrimerNombre',
                            allowBlank: false
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldLabel: 'PrimerApellido',
                            name: 'PrimerApellido',
                            allowBlank: false
                        }
                    ],
                    buttons:[
                        {
                            xtype: 'button',
                            text: 'Guardar',
                            name: 'btnGuardar',
                            formBind: true
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});