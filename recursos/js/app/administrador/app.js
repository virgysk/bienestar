Ext.Loader.setConfig({
	enabled : true
});

Ext.application({
	name : 'administrador',
	appFolder :  bienestar.base_url+'recursos/js/app/administrador',
	controllers : [
		'ConPrincipal','ConUsuario'
		],
	launch : function() {
		Ext.create('Ext.container.Viewport', {
			layout : 'fit',
			items : [{
				xtype : 'visprincipal'
			}]
		}
		);
	}
});