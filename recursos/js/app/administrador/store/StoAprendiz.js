Ext.define('administrador.store.StoAprendiz',{
        extend: 'Ext.data.Store',
        autoLoad: true,
        pageSize: 20,
        model: 'administrador.model.ModelAprendiz',
        proxy: {
            SimpleSortMode:true,
            type: 'ajax',
            api: {
                read: bienestar.Utilidades.urlListarAprendiz
            },
            actionMethods: {
                read    : 'POST'
            },
            reader: {
                type: 'json',
                root: 'datos',
                successProperty: 'success'
            }
        }
});