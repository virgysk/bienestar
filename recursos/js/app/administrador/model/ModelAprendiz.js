Ext.define('administrador.model.ModelAprendiz',{
        extend: 'Ext.data.Model',
        fields: [
        {
        	name:'NombrePrograma',
        	type: 'string'
        },{
        	name:'PrimerNombre',
        	type: 'string'
        },{
                name:'SegundoNombre',
                type: 'string'
        },{
        	name:'PrimerApellido',
        	type: 'string'
        },{
                name:'SegundoApellido',
                type: 'string'
        },{
        	name:'DocumentoIdentidad',
        	type: 'int'
        },{
                name:'FechaNacimiento',
                type: 'string'
        },{
                name:'FechaIngreso',
                type: 'string'
        },{
                name:'Direccion',
                type: 'string'
        },{
                name:'Telefono',
                type: 'string'
        },{
                name:'Email',
                type: 'string'
        }
        ]
});