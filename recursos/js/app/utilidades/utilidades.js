Ext.define('administrador.utilidades.utilidades.js',{
    alternateClassName:'bienestar.Utilidades',
    singleton:true,

    /*urls*/
    urlListarAprendiz: bienestar.base_url + 'index.php/conprincipal/listarAprendiz',
    urlCerrarSesion: bienestar.base_url + 'index.php/conprincipal/cerrarSesion',

    urlImportarAprendiz: bienestar.base_url + 'index.php/conprincipal/importarAprendiz',
    urlExportarAprendiz: bienestar.base_url + 'index.php/conprincipal/exportarAprendiz',


    /*Iconos*/
    iconoGestionParametros: bienestar.base_url + 'recursos/imagenes/gestion_parametros.png',
    iconoCerrarSesion: bienestar.base_url + 'recursos/imagenes/salir.png',
    iconoGestionUsuarios: bienestar.base_url + 'recursos/imagenes/usuarios.png',
    iconoLogin: bienestar.base_url + 'recursos/imagenes/login.png',

    iconoEdicionGrilla: bienestar.base_url + 'recursos/imagenes/iconos/editar.png',
    iconoEliminacionGrilla: bienestar.base_url + 'recursos/imagenes/iconos/eliminar.png',
    iconoInsercionGrilla: bienestar.base_url + 'recursos/imagenes/iconos/insertar.png',
    iconoImportacionGrilla: bienestar.base_url + 'recursos/imagenes/iconos/importar.png',
     iconoPdf: bienestar.base_url + 'recursos/imagenes/iconos/pdf.png'

});