bienestar.cerrarSesion = function(){

			Ext.Ajax.request({
                type: 'post',
				url: bienestar.urlcerrarSesion,
				success:function(respuesta, peticion){
					var obj = Ext.decode(respuesta.responseText);
					if(obj.success){
						location.href = bienestar.base_url;
					}
				},
				error: function(respuesta, peticion){
					console.log(respuesta);
				}
			});
}